FROM node:15.5.1-alpine3.10 AS build
ARG profile=prod
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN ./node_modules/@angular/cli/bin/ng build --$profile

FROM nginx
EXPOSE 80
COPY nginx.conf /etc/nginx/nginx.conf
WORKDIR /usr/share/nginx/html
COPY --from=build /usr/src/app/dist/workout-frontend/ .
