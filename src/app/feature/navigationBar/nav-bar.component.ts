import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {TokenStorage} from '../../configuration/security/token.storage';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../../configuration/security/authentication.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  isLoggedIn$: Observable<boolean> = this.tokenStorage.isLoggedIn$;

  constructor(private router: Router, private tokenStorage: TokenStorage, private authService: AuthenticationService) {
  }

  ngOnInit(): void {
  }

  onLogOut(): void {
    this.authService.signout();
  }

  isUserAClient(): boolean {
    return this.tokenStorage.getRole() === 'ROLE_CLIENT';
  }

}
