import {Component} from '@angular/core';
import {FriendsService} from '../../friends/service/friends.service';
import {MatDialogRef} from '@angular/material/dialog';
import {UserAccount} from '../../home/model/user-account';
import {InvitationService} from '../service/invitation.service';

@Component({
  selector: 'app-add-member-dialog',
  templateUrl: './addFriendDialog.html'
})

export class AddFriendDialogComponent {

  nonFriends: UserAccount[] = [];

  constructor(private friendsService: FriendsService, private matDialogRef: MatDialogRef<AddFriendDialogComponent>,
              private invitationService: InvitationService) {
    this.getNonConnectedUsers();
  }

  getNonConnectedUsers(): void {
    this.friendsService.getNonConnectedUser().subscribe(
      response => {
        this.nonFriends = response;
      },
      error1 => alert('Error finding non connected users')
    );
  }

  addFriend(userId: number): void {
    this.invitationService.addFriend(userId).subscribe(
      response => {
        alert('User invited');
        for (let i = 0; i < this.nonFriends.length; i++) {
          if (this.nonFriends[i].id === userId) {
            this.nonFriends.splice(i, 1);
            break;
          }
        }

      }
    );
  }

}
