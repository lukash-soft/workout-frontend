import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {InvitationDto} from '../model/InvitationDto';
import {Invitation} from '../model/invitation';

@Injectable({
  providedIn: 'root'
})
export class InvitationService {

  private INVITATION_URL = `${environment.baseUrl}/invitations`;

  constructor(private http: HttpClient) {
  }

  public getAllInvitations(): Observable<InvitationDto> {
    return this.http.get<InvitationDto>(this.INVITATION_URL);
  }

  public addFriend(userId: number): Observable<Invitation> {
    const urlId = `${this.INVITATION_URL}/${userId}`;
    return this.http.post<Invitation>(urlId, null);
  }

  public cancelInvitation(invitationId: number): Observable<void> {
    const invitationIdUrl = `${this.INVITATION_URL}/cancel/${invitationId}`;
    return this.http.delete<void>(invitationIdUrl);
  }

  public declineInvitation(invitationIdDecline: number): Observable<Invitation> {
    const declineUrl = `${this.INVITATION_URL}/decline/${invitationIdDecline}`;
    return this.http.put<Invitation>(declineUrl, invitationIdDecline);
  }

  public acceptInvitation(invitationIdAccept: number): Observable<Invitation> {
    const acceptUrl = `${this.INVITATION_URL}/accept/${invitationIdAccept}`;
    return this.http.put<Invitation>(acceptUrl, invitationIdAccept);
  }

  public removeFriend(friendId: number): Observable<void> {
    const removeUrl = `${this.INVITATION_URL}/friend/${friendId}`;
    return this.http.delete<void>(removeUrl);
  }
}
