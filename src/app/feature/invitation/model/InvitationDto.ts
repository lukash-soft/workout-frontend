import {Invitation} from './invitation';

export class InvitationDto {

  sent: Invitation[];
  received: Invitation[];


  constructor(sent: Invitation[], received: Invitation[]) {
    this.sent = sent;
    this.received = received;
  }
}
