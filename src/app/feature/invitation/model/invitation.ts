import {UserAccount} from '../../home/model/user-account';

export class Invitation {

  id: number;
  accountFrom: UserAccount;
  accountTo: UserAccount;
  created: Date;
  edited: Date;
  statusInv: string;


  constructor(id: number, accountFrom: UserAccount, accountTo: UserAccount, created: Date, edited: Date, statusInv: string) {
    this.id = id;
    this.accountFrom = accountFrom;
    this.accountTo = accountTo;
    this.created = created;
    this.edited = edited;
    this.statusInv = statusInv;
  }
}
