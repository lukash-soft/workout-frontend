import {Component, OnInit, ViewChild} from '@angular/core';
import {Invitation} from './model/invitation';
import {InvitationService} from './service/invitation.service';
import {MatTable} from '@angular/material/table';
import {UserAccount} from '../home/model/user-account';
import {isLineBreak} from 'codelyzer/angular/sourceMappingVisitor';
import {MatDialog} from '@angular/material/dialog';
import {AddFriendDialogComponent} from './addFriendDialog/addFriendDailog';

@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.css']
})


export class InvitationComponent implements OnInit {

  @ViewChild('sentTable') sentTable: MatTable<any>;

  @ViewChild('receivedTable') receivedTable: MatTable<any>;

  columnsToDisplay: string[] = ['firstName', 'lastName', 'birthDate', 'action'];
  sentInvitations: Invitation[] = [];
  receivedInvitations: Invitation[] = [];

  constructor(private invitationService: InvitationService, private matDialog: MatDialog) {
    this.getAllInvitations();
  }

  ngOnInit(): void {
  }

  getAllInvitations(): void {
    this.invitationService.getAllInvitations().subscribe(
      response => {
        this.sentInvitations = response.sent;
        this.receivedInvitations = response.received;
      },
      error => alert('Error finding invitations')
    );
  }

  openAddFriendDialog(): void {
    const dialogRef = this.matDialog.open(AddFriendDialogComponent);
    dialogRef.afterClosed().subscribe(
      (friendAdded) =>
        this.addFriend(friendAdded.id)
    );
  }

  private addFriend(userId: number): void {
    this.invitationService.addFriend(userId).subscribe(
      info => alert('User invited'),
      error1 => alert('error')
    );
  }

  cancelInvitation(invitationId: number): void {
    this.invitationService.cancelInvitation(invitationId).subscribe(
      response => {
        for (let i = 0; i < this.sentInvitations.length; i++) {
          if (this.sentInvitations[i].id === invitationId) {
            this.sentInvitations.splice(i, 1);
            this.sentTable.renderRows();
            break;
          }
        }
      },
      error => alert('Invitation error')
    );
  }

  declineInvitation(invitationId: number): void {
    this.invitationService.declineInvitation(invitationId).subscribe(
      response => {
        for (let i = 0; i < this.receivedInvitations.length; i++) {
          if (this.receivedInvitations[i].id === invitationId) {
            this.receivedInvitations.splice(i, 1);
            this.receivedTable.renderRows();
            break;
          }
        }
      },
      error => alert('error')
    );
  }

  acceptInvitation(invitationId: number): void {
    this.invitationService.acceptInvitation(invitationId).subscribe(
      response => {
        for (let i = 0; i < this.receivedInvitations.length; i++) {
          if (this.receivedInvitations[i].id === invitationId) {
            this.receivedInvitations.splice(i, 1);
            this.receivedTable.renderRows();
            break;
          }
        }
      },
      error => alert('error adding friends')
    );
  }
}
