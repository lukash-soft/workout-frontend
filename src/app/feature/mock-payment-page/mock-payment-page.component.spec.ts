import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MockPaymentPageComponent } from './mock-payment-page.component';

describe('MockPaymentPageComponent', () => {
  let component: MockPaymentPageComponent;
  let fixture: ComponentFixture<MockPaymentPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MockPaymentPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MockPaymentPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
