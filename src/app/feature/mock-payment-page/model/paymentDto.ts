export class PaymentDto {
  constructor(id: number, cardNumber: string, expirationDate: Date, securityCode: string, amountPaid: number, invoiceId: number) {
    this.id = id;
    this.cardNumber = cardNumber;
    this.expirationDate = expirationDate;
    this.securityCode = securityCode;
    this.amountPaid = amountPaid;
    this.invoiceId = invoiceId;
  }
  id: number;
  cardNumber: string;
  expirationDate: Date;
  securityCode: string;
  amountPaid: number;
  invoiceId: number;


}
