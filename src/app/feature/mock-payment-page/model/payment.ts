import {UserAccountDto} from '../../calendar/model/UserAccountDto';

export class Payment {
  id: number;
  paymentDate: Date;
  amount: number;
  cardNumber: number;
  user: UserAccountDto;


  constructor(id: number, paymentDate: Date, amount: number, cardNumber: number, user: UserAccountDto) {
    this.id = id;
    this.paymentDate = paymentDate;
    this.amount = amount;
    this.cardNumber = cardNumber;
    this.user = user;
  }
}
