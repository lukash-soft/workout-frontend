import { TestBed } from '@angular/core/testing';

import { MockPaymentPageService } from './mock-payment-page.service';

describe('MockPaymentPageService', () => {
  let service: MockPaymentPageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MockPaymentPageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
