import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RegisterModel} from '../model/register-model';
import {environment} from '../../../../environments/environment';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  private REGISTER_URL = `${environment.baseUrl}/register`;

  constructor(private http: HttpClient, private router: Router) {}

  public registerUser(dto: RegisterModel): Observable<void>{
    return this.http.post<void>(this.REGISTER_URL, dto);
  }
}
