
export class RegisterModel {

  firstName: string;
  lastName: string;
  email: string;
  birthDate: Date;
  password: string;
  role: string;


  constructor(firstName: string, lastName: string, email: string, birthDate: Date, password: string, role: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.birthDate = birthDate;
    this.password = password;
    this.role = role;
  }
}
