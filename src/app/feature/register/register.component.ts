import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RegisterService} from './service/register.service';
import {RegisterModel} from './model/register-model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {

  form: FormGroup;


  constructor(private registerService: RegisterService, private router: Router) {
    this.form = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      birthDate: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required, Validators.minLength(5)]),
      role: new FormControl('', Validators.required)
    });
  }

  ngOnInit(): void {
  }

  onRegister(): void {
    if (this.form.controls.birthDate.value.getTime() >= Date.now()) {
      alert('You entered wrong date!');
      return;
    }
    const registerValues = new RegisterModel(
      this.form.controls.firstName.value,
      this.form.controls.lastName.value,
      this.form.controls.email.value,
      this.form.controls.birthDate.value,
      this.form.controls.password.value,
      this.form.controls.role.value);
    this.registerService.registerUser(registerValues).subscribe(
      () => this.router.navigate(['/']),
      error => alert('error'));
  }
}

