import {Component, Inject} from '@angular/core';
import {Post} from '../../model/post';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';


@Component({
  selector: 'app-update-post-dialog',
  templateUrl: './updatePostDialog.html'
})


export class UpdatePostDialogComponent {

  formPost: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Post,
              private dialogRef: MatDialogRef<UpdatePostDialogComponent>) {
    this.formPost = new FormGroup({
      id: new FormControl(data.id, Validators.required),
      title: new FormControl(data.title, Validators.required),
      content: new FormControl(data.content, Validators.required),
      }
    );
  }

  onUpdateAndSave(): void {
    const postToUpdate = Post.PostForm(
      this.formPost.controls.id.value,
      this.formPost.controls.title.value,
      this.formPost.controls.content.value,
    );
    this.dialogRef.close(postToUpdate);
  }
}
