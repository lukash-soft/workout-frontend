import {Component} from '@angular/core';
import {Post} from '../../model/post';
import {MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-create-post-dialog',
    templateUrl: './createPostDialog.html',
  }
)

export class CreatePostDialogComponent {


  formPost: FormGroup;

  constructor(public matDialogRef: MatDialogRef<CreatePostDialogComponent>) {
    this.formPost = new FormGroup({
        id: new FormControl('', Validators.required),
        title: new FormControl('', Validators.required),
        content: new FormControl('', Validators.required),
      }
    );
  }

  createPost(): void {
    const postToCreate = Post.PostForm(
      this.formPost.controls.id.value,
      this.formPost.controls.title.value,
      this.formPost.controls.content.value,
    );
    this.matDialogRef.close(postToCreate);
  }

}
