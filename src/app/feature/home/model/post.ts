import {UserAccount} from './user-account';
import {toTitleCase} from 'codelyzer/util/utils';

export class Post {

  id: number;
  title: string;
  content: string;
  created: string;
  creator: UserAccount;


  constructor() {
  }

  static PostForm(id: number, title: string, content: string): Post {
    const postForm = new Post();

    postForm.id = id;
    postForm.title = title;
    postForm.content = content;

    return postForm;
  }

}
