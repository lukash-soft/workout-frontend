export class UserAccount {

  id: number;
  firstName: string;
  lastName: string;
  birthDate: Date;
  email: string;

  constructor(id: number, firstName: string, lastName: string, birthDate: Date, email: string) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDate = birthDate;
    this.email = email;
  }
}
