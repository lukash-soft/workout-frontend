import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {UserAccount} from '../model/user-account';
import {Post} from '../model/post';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private readonly HOME_URL = `${environment.baseUrl}/accounts/details`;
  private readonly POST_URL = `${environment.baseUrl}/posts`;
  private readonly PICTURE_URL = `${environment.baseUrl}/pictures`;

  constructor(private http: HttpClient) {
  }

  public getAccountDetails(): Observable<UserAccount> {
    return this.http.get<UserAccount>(this.HOME_URL);
  }

  public getAllPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.POST_URL);
  }

  public createPost(createdPost: Post): Observable<Post> {
    return this.http.post<Post>(this.POST_URL, createdPost);
  }

  public updateUserDetails(userAccount: UserAccount): Observable<UserAccount>{
    const updateUrl = `${environment.baseUrl}/accounts`;
    return this.http.put<UserAccount>(updateUrl, userAccount);
  }

  public deletePost(postId: number): Observable<void> {
    const DELETE_URL = `${this.POST_URL}/${postId}`;
    return this.http.delete<void>(DELETE_URL);
  }

  public updatePost(postToUpdate: Post): Observable<Post> {
    return this.http.put<Post>(this.POST_URL, postToUpdate);
  }

  public getProfilePicture(): Observable<Blob> {
    return this.http.get(`${this.PICTURE_URL}/profile`, {responseType: 'blob'});
  }
}
