import {AfterViewInit, Component, OnInit} from '@angular/core';
import {UserAccount} from './model/user-account';
import {HomeService} from './service/home.service';
import {Post} from './model/post';
import {CreatePostDialogComponent} from './dialog.models/dialog.createPost/createPostDialog';
import {MatDialog} from '@angular/material/dialog';
import {UpdatePostDialogComponent} from './dialog.models/dialog.updatePost/updatePostDialog';
import {FileUploader} from 'ng2-file-upload';
import {environment} from '../../../environments/environment';
import {TokenStorage} from '../../configuration/security/token.storage';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {

  private readonly backupProfile = '/assets/images/blank-profile.png';
  userAccount: UserAccount = new UserAccount(Number(''), ' ', ' ', new Date(), ' ');
  posts: Post[] = [];
  uploader: FileUploader;
  profilePic: any;

  constructor(private homeService: HomeService, private dialog: MatDialog,
              private tokenStorage: TokenStorage, private sanitizer: DomSanitizer) {
    this.onGetUserData();
    this.getAllPosts();
  }

  ngOnInit(): void {
    this.uploader = new FileUploader(
      {
        url: `${environment.baseUrl}/pictures/profile`,
        removeAfterUpload: true,
        autoUpload: true,
        headers: [
          {name: 'Accept', value: 'application/json, text/plain, */*'},
          {name: 'authorization', value: this.tokenStorage.getToken().jwt}
        ],
      });
    this.loadProfilePic();
  }

  private loadProfilePic(): void {
    this.homeService.getProfilePicture().subscribe(
      response => {
        if (response.size > 0) {
          this.profilePic = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(response));
        } else {
          this.profilePic = this.backupProfile;
        }
      },
      err => {
        console.error(err);
        this.profilePic = this.backupProfile;
      }
    );
  }

  ngAfterViewInit(): void {
    // cors issue workaround
    this.uploader.onAfterAddingFile = (item => {
      item.withCredentials = false;
    });
    this.uploader.onSuccessItem = (item, response, status) => {
      if (status === 200) {
        this.loadProfilePic();
      } else {
        console.error(response);
      }
    };
  }

  onGetUserData(): void {
    this.homeService.getAccountDetails().subscribe(
      response => {
        this.userAccount = response;
      },
      error1 => alert('err'));
  }

  getAllPosts(): void {
    this.homeService.getAllPosts().subscribe(
      response => {
        this.posts = response;
      },
      error => alert('error getting posts'));
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreatePostDialogComponent);
    dialogRef.afterClosed().subscribe(
      (postToCreate) => {
        if (!postToCreate.content) {
          alert('Content is empty. Please fill the content.');
        }else{
          this.createPost(postToCreate);
        }
      }
    );
  }

  private createPost(postToCreate: Post): void {
    this.homeService.createPost(postToCreate).subscribe(
      response => {
        this.posts.push(response);
      },
      error => alert('Error creating post')
    );
  }

  deletePost(postId: number): void {
    if (!confirm('Do you want to delete?')) {
      return;
    }
    this.removePostFromUi(postId);
  }

  private removePostFromUi(postId: number): void {
    this.homeService.deletePost(postId).subscribe(
      response => {
        for (let i = 0; i < this.posts.length; i++) {
          if (this.posts[i].id === postId) {
            this.posts.splice(i, 1);
            break;
          }
        }
      });
  }

  openUpdateDialog(postToEdit: Post): void {
    const dialogRef = this.dialog.open(UpdatePostDialogComponent, {
      data: postToEdit
    });
    dialogRef.afterClosed().subscribe(
      (editedPost) => {
        if (!editedPost) {
          return;
        }
        this.homeService.updatePost(editedPost).subscribe(
          response => {
            for (let i = 0; i < this.posts.length; i++) {
              if (this.posts[i].id === editedPost.id) {
                this.posts.splice(i, 1, response);
                break;
              }
            }
          },
          error => alert('Error updating post')
        );
      });
  }
}
