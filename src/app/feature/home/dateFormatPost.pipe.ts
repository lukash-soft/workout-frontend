import {Pipe, PipeTransform} from '@angular/core';
import {valueReferenceToExpression} from '@angular/compiler-cli/src/ngtsc/annotations/src/util';
import {min} from 'rxjs/operators';

@Pipe({
  name: 'datePipeFormat'
})
export class DateFormatPostPipe implements PipeTransform {

  transform(value: any): any {
    const year = value[0];
    const month = value[1];
    const day = value[2];
    const hours = value[3];
    const minutes = value[4];
    const minutesToDisplay = minutes < 10 ? `0${minutes}` : minutes;


    return `This post was created at ${hours}:${minutesToDisplay}
     on the ${day}-${month}-${year}`;

  }

}
