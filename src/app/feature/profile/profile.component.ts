import {Component, ViewChild} from '@angular/core';
import {ProfileService} from './service/profile.service';
import {AthleteDetails} from './model/athlete-details';
import {MatDialog} from '@angular/material/dialog';
import {AthleteDetailsDialogUpdateComponent} from './dialog.athleteDetails/updateDetailsDialog/athleteDetailsDialogUpdate';
import {MatTable} from '@angular/material/table';
import {AthleteDetailsDialogAddComponent} from './dialog.athleteDetails/addAthleteDetails/athlete-details-dialog-add.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {

  // athleteDetails: AthleteDetails = new AthleteDetails(null, new Date(), 0, 0, 0, 0, 0, 0, 0);
  columnsToDisplay = ['added', 'weight', 'height', 'benchPress', 'squat', 'waist', 'arm', 'thigh', 'updateButton', 'deleteButton'];
  athleteDetailsArray: AthleteDetails[] = [];

  @ViewChild('detailsTable') detailsTable: MatTable<any>;

  constructor(private profileService: ProfileService, private matDialog: MatDialog) {
    this.getAthleteDetails();
  }

  getAthleteDetails(): void {
    this.profileService.getAthleteDetails().subscribe(
      response => {
        this.athleteDetailsArray = response;
      },
      error => alert('error when getting the details')
    );
  }

  openAddDialog(): void {
    const dialogRef = this.matDialog.open(AthleteDetailsDialogAddComponent);
    dialogRef.afterClosed().subscribe(
      athleteDetails => {
        if (!athleteDetails) {
          return;
        }
        this.profileService.addAthleteDetails(athleteDetails).subscribe(
          response => {
            this.athleteDetailsArray.push(response);
            this.detailsTable.renderRows();
          }
        );
      },
      error => alert('Error adding Athlete Details.')
    );
  }

  openUpdateDialog(athleteDetails: AthleteDetails): void {
    const dialogRef = this.matDialog.open(AthleteDetailsDialogUpdateComponent, {
      data: athleteDetails,
    });
    dialogRef.afterClosed().subscribe((updatedAthleteDetails) => {
      this.profileService.updateAthleteDetails(updatedAthleteDetails).subscribe(
        response => {
          for (let i = 0; i < this.athleteDetailsArray.length; i++) {
            if (this.athleteDetailsArray[i].id === updatedAthleteDetails.id) {
              this.athleteDetailsArray.splice(i, 1, response);
              this.detailsTable.renderRows();
              break;
            }
          }
        },
        error => alert('Error')
      );
    });
  }

  deleteDetails(detailsId: number): void {
    this.profileService.deleteAthleteDetails(detailsId).subscribe(
      () => {
        for (let i = 0; i < this.athleteDetailsArray.length; i++) {
          if (this.athleteDetailsArray[i].id === detailsId) {
            this.athleteDetailsArray.splice(i, 1);
            this.detailsTable.renderRows();
            break;
          }
        }
      }
    );
  }
}

