import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AthleteDetails} from '../model/athlete-details';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private DETAILS_URL = `${environment.baseUrl}/athleteDetails`;

  constructor(private http: HttpClient) {
  }


  public getAthleteDetails(): Observable<AthleteDetails[]> {
    return this.http.get<AthleteDetails[]>(this.DETAILS_URL);
  }

  public updateAthleteDetails(athleteDetails: AthleteDetails): Observable<AthleteDetails> {
    return this.http.put<AthleteDetails>(this.DETAILS_URL, athleteDetails);
  }

  public addAthleteDetails(athleteDetails: AthleteDetails): Observable<AthleteDetails> {
    return this.http.post<AthleteDetails>(this.DETAILS_URL, athleteDetails);
  }

  public deleteAthleteDetails(detailsId: number): Observable<void> {
    return this.http.delete<void>(`${this.DETAILS_URL}/${detailsId}`);
  }
}


