import {Component, Inject} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AthleteDetails} from '../../model/athlete-details';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-athlete-details-update-dialog',
  templateUrl: './athleteDetailsDialogUpdate.html',
})
export class AthleteDetailsDialogUpdateComponent {

  formAthleteDetails: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public athleteDetails: AthleteDetails,
              private dialogRef: MatDialogRef<AthleteDetailsDialogUpdateComponent>) {
    this.formAthleteDetails = new FormGroup({
      id: new FormControl(athleteDetails.id, Validators.required),
      added: new FormControl(athleteDetails.added, Validators.required),
      weight: new FormControl(athleteDetails.weight, Validators.required),
      height: new FormControl(athleteDetails.height, Validators.required),
      benchPress: new FormControl(athleteDetails.benchPress, Validators.required),
      waistMeasurement: new FormControl(athleteDetails.waistMeasurement, Validators.required),
      armMeasurement: new FormControl(athleteDetails.armMeasurement, Validators.required),
      thighMeasurement: new FormControl(athleteDetails.thighMeasurement, Validators.required),
      squat: new FormControl(athleteDetails.squat, Validators.required),
    });
  }

  onUpdateAndSave(): void {
    const athleteDetailsValues = new AthleteDetails(
      this.formAthleteDetails.controls.id.value,
      this.formAthleteDetails.controls.added.value,
      this.formAthleteDetails.controls.weight.value,
      this.formAthleteDetails.controls.height.value,
      this.formAthleteDetails.controls.benchPress.value,
      this.formAthleteDetails.controls.waistMeasurement.value,
      this.formAthleteDetails.controls.armMeasurement.value,
      this.formAthleteDetails.controls.thighMeasurement.value,
      this.formAthleteDetails.controls.squat.value,
    );
    this.dialogRef.close(athleteDetailsValues);
  }
}

