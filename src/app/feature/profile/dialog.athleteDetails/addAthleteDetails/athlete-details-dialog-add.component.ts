import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AthleteDetails} from '../../model/athlete-details';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-athlete-details-add-dialog',
  templateUrl: './athlete-details-dialog-add.component.html'
})

export class AthleteDetailsDialogAddComponent {

  formAthleteDetails: FormGroup;


  constructor(public dialogRef: MatDialogRef<AthleteDetailsDialogAddComponent>) {
    this.formAthleteDetails = new FormGroup({
      id: new FormControl(null, Validators.required),
      added: new FormControl(Date.now(), Validators.required),
      weight: new FormControl('', Validators.required),
      height: new FormControl('', Validators.required),
      benchPress: new FormControl('', Validators.required),
      waistMeasurement: new FormControl('', Validators.required),
      armMeasurement: new FormControl('', Validators.required),
      thighMeasurement: new FormControl('', Validators.required),
      squat: new FormControl('', Validators.required),
    });
  }

  onAdd(): void {
    const athleteDetailsToAdd = new AthleteDetails(
      this.formAthleteDetails.controls.id.value,
      this.formAthleteDetails.controls.added.value,
      this.formAthleteDetails.controls.weight.value,
      this.formAthleteDetails.controls.height.value,
      this.formAthleteDetails.controls.benchPress.value,
      this.formAthleteDetails.controls.waistMeasurement.value,
      this.formAthleteDetails.controls.armMeasurement.value,
      this.formAthleteDetails.controls.thighMeasurement.value,
      this.formAthleteDetails.controls.squat.value,
    );
    this.dialogRef.close(athleteDetailsToAdd);
  }


}
