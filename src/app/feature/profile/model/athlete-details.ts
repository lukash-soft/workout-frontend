export class AthleteDetails {

  id: number;
  added: Date;
  weight: number;
  height: number;
  benchPress: number;
  waistMeasurement: number;
  armMeasurement: number;
  thighMeasurement: number;
  squat: number;


  constructor(id: number, added: Date, weight: number, height: number, benchPress: number, waistMeasurement: number, armMeasurement: number, thighMeasurement: number, squat: number) {
    this.id = id;
    this.added = added;
    this.weight = weight;
    this.height = height;
    this.benchPress = benchPress;
    this.waistMeasurement = waistMeasurement;
    this.armMeasurement = armMeasurement;
    this.thighMeasurement = thighMeasurement;
    this.squat = squat;
  }
}
