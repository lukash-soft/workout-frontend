import {UserAccountDto} from '../../calendar/model/UserAccountDto';

export class Offer {

  id: number;
  title: string;
  description: string;
  price: number;
  coach: UserAccountDto;

  constructor(id: number, title: string, description: string, price: number, coach: UserAccountDto) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.price = price;
    this.coach = coach;
  }
}
