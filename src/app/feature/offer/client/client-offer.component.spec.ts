import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientOfferComponent } from './client-offer.component';

describe('ClientOfferComponent', () => {
  let component: ClientOfferComponent;
  let fixture: ComponentFixture<ClientOfferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientOfferComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
