import {Component} from '@angular/core';
import {OfferService} from '../coach/service/offer.service';
import {OfferDto} from '../model/offerDto';
import {SubscriptionService} from '../../subscription/service/subscription.service';
import {Subscription} from '../../subscription/model/subscription';

@Component({
  selector: 'app-client-offer',
  templateUrl: './client-offer.component.html',
  styleUrls: ['./client-offer.component.css']
})
export class ClientOfferComponent {

  offers: OfferDto[] = [];
  subscriptions: Subscription[] = [];

  constructor(private offerService: OfferService, private subscriptionService: SubscriptionService) {
    this.getOffersAsAClient();
    this.getSubsForUser();
  }


  getSubsForUser(): void {
    this.subscriptionService.getAllSubscription().subscribe(
      response => {
        this.subscriptions = response;
      },
      error => alert('Error getting subs')
    );
  }

  getOffersAsAClient(): void {
    this.offerService.getOffers().subscribe(
      response => {
        this.offers = response;
      },
      err => alert('Error getting offers for client')
    );
  }

  createSubscription(offerDto: OfferDto): void {
    this.subscriptionService.createSubscription(offerDto).subscribe(
      response => {
        this.subscriptions.push(response);
        for (let i = 0; i < this.offers.length; i++) {
          if (this.offers[i].id === offerDto.id) {
            this.offers.splice(i, 1);
          }
        }
      },
      err => alert('Error creating subs')
    );
  }


}
