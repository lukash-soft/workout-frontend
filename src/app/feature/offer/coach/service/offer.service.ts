import {Injectable} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Offer} from '../../model/offer';

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  private BASE_URL = `${environment.baseUrl}/offer`;

  constructor(private http: HttpClient) {
  }

  public getOffers(): Observable<Offer[]> {
    return this.http.get<Offer[]>(this.BASE_URL);
  }

  public createOffer(offer: Offer): Observable<Offer> {
    return this.http.post<Offer>(this.BASE_URL, offer);
  }

  public updateOffer(offer: Offer): Observable<Offer> {
    return this.http.put<Offer>(this.BASE_URL, offer);
  }

  public deleteOffer(offerId: number): Observable<void> {
    return this.http.delete<void>(`${this.BASE_URL}/${offerId}`);
  }

}
