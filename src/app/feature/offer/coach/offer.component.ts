import {Component} from '@angular/core';
import {Offer} from '../model/offer';
import {MatDialog} from '@angular/material/dialog';
import {OfferService} from './service/offer.service';
import {OfferDialogComponent} from './dialog/offerDialogComponent';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class OfferComponent {

  offers: Offer[] = [];

  constructor(private matDialog: MatDialog, private offerService: OfferService) {
    this.getOfferForCoach();
  }

  getOfferForCoach(): void {
    this.offerService.getOffers().subscribe(
      response => {
        this.offers = response;
      },
      error => alert('Error getting offers')
    );
  }

  createOffer(): void {
    const dialogRef = this.matDialog.open(OfferDialogComponent);
    dialogRef.afterClosed().subscribe(
      offerToCreate => {
        this.offerService.createOffer(offerToCreate).subscribe(
          response => {
            this.offers.push(response);
          },
          error => alert('Error creating offer.')
        );
      }
    );
  }

  deleteOffer(offerId: number): void {
    this.offerService.deleteOffer(offerId).subscribe(
      () => {
        for (let i = 0; i < this.offers.length; i++) {
          if (this.offers[i].id === offerId) {
            this.offers.splice(i, 1);
            break;
          }
        }
      },
      error => alert('Error deleting offer')
    );
  }

  updateOffer(offer: Offer): void {
    const dialogRef = this.matDialog.open(OfferDialogComponent, {
      data: offer
    });
    dialogRef.afterClosed().subscribe(
      (updatedOffer) =>
        this.offerService.updateOffer(updatedOffer).subscribe(
          response => {
            for (let i = 0; i < this.offers.length; i++) {
              if (this.offers[i].id === updatedOffer.id) {
                this.offers.splice(i, 1, response);
                break;
              }
            }
          },
          error => alert('Error updating offer.')
        )
    );
  }

}
