import {Component, Inject} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {OfferDto} from '../../model/offerDto';
import {Offer} from '../../model/offer';


@Component({
  selector: 'app-offer-dialog',
  templateUrl: './offerDialogComponent.html'
})
export class OfferDialogComponent {

  formCreateOffer: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public offer: Offer, private matDialogRef: MatDialogRef<OfferDialogComponent>) {
    if (offer === null) {
      this.prepareEmptyForm();
    } else {
      this.fillFormWithExistingOffer(offer);
    }
  }


  private fillFormWithExistingOffer(offer: Offer): void {
    this.formCreateOffer = new FormGroup({
      title: new FormControl(offer.title, Validators.required),
      description: new FormControl(offer.description, Validators.required),
      price: new FormControl(offer.price, Validators.required)
    });
  }

  private prepareEmptyForm(): void {
    this.formCreateOffer = new FormGroup({
        title: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required),
        price: new FormControl('', Validators.required),
      }
    );
  }

  completeOfferForm(): void {
    const offerId = this.offer !== null ? this.offer.id : null;

    const offerToSave = new Offer(
      offerId,
      this.formCreateOffer.controls.title.value,
      this.formCreateOffer.controls.description.value,
      this.formCreateOffer.controls.price.value,
      this.offer.coach
    );
    this.matDialogRef.close(offerToSave);
  }

  onSave(): void {
    const offerId = this.offer !== null ? this.offer.id : null;

    const offerToSave = new OfferDto(
      offerId,
      this.formCreateOffer.controls.title.value,
      this.formCreateOffer.controls.description.value,
      this.formCreateOffer.controls.price.value
    );
    this.matDialogRef.close(offerToSave);
  }
}
