import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {UserAccount} from '../../home/model/user-account';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class FriendsService {
  private ACCOUNT_URL = `${environment.baseUrl}/accounts/friends`;
  private NON_CONNECTED_URL = `${environment.baseUrl}/accounts/nonConnectedUsers`;

  constructor(private http: HttpClient) {

  }

  public getFriendsForUser(): Observable<UserAccount[]>{
    return this.http.get<UserAccount[]>(this.ACCOUNT_URL);
  }

  public getNonConnectedUser(): Observable<UserAccount[]>{
    return this.http.get<UserAccount[]>(this.NON_CONNECTED_URL);
  }

}
