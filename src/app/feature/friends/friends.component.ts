import {Component, OnInit, ViewChild} from '@angular/core';
import {UserAccount} from '../home/model/user-account';
import {FriendsService} from './service/friends.service';
import {InvitationService} from '../invitation/service/invitation.service';
import {MatTable} from '@angular/material/table';


@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit {

  columnsToDisplay = ['firstName', 'lastName', 'birthDate', 'email', 'action'];
  friends: UserAccount[] = [];
  nonFriends: UserAccount[] = [];

  @ViewChild('friendsTable') friendsTable: MatTable<any>;

  constructor(private friendsService: FriendsService, private invitationService: InvitationService) {
    this.getNonConnectedUsers();
    this.getFriendsOfUser();
  }

  ngOnInit(): void {
  }

  getFriendsOfUser(): void {
    this.friendsService.getFriendsForUser().subscribe(
      response => {
        this.friends = response;
      },
      error1 => alert('Error getting your friends')
    );
  }

  getNonConnectedUsers(): void {
    this.friendsService.getNonConnectedUser().subscribe(
      response => {
        this.nonFriends = response;
      },
      error1 => alert('Error finding non connected users')
    );
  }

  addFriend(userId: number): void {
    this.invitationService.addFriend(userId).subscribe(
      info => alert('User invited'),
      error1 => alert('error')
    );
  }

  removeFriend(friendId: number): void {
    this.invitationService.removeFriend(friendId).subscribe(
      response => {
        for (let i = 0; i < this.friends.length; i++) {
          if (this.friends[i].id === friendId) {
            this.friends.splice(i, 1);
            this.friendsTable.renderRows();
            break;
          }
        }
      }
    );

  }
}
