import {Component, Inject} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UserAccount} from '../../home/model/user-account';
import {UserSettingsAccountDto} from '../model/UserSettingsAccountDto';
import * as moment from 'moment';

@Component({
  selector: 'app-settings-dialog',
  templateUrl: './settingsDialogComponent.html',
})
export class SettingsDialogComponent {

  formSettings: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public userAccount: UserAccount,
              private dialogRef: MatDialogRef<SettingsDialogComponent>) {
    this.formSettings = new FormGroup({
      firstName: new FormControl(userAccount.firstName, Validators.required),
      lastName: new FormControl(userAccount.lastName, Validators.required),
      birthDate: new FormControl(userAccount.birthDate, Validators.required),
    });
  }

  onUpdate(): void {
    const settingsValues = new UserSettingsAccountDto(
      this.formSettings.controls.firstName.value,
      this.formSettings.controls.lastName.value,
      new Date(moment(this.formSettings.controls.birthDate.value).format('YYYY-MM-DD')),
    );
    this.dialogRef.close(settingsValues);
  }
}
