import {Component} from '@angular/core';
import {UserAccount} from '../home/model/user-account';
import {HomeService} from '../home/service/home.service';
import {MatDialog} from '@angular/material/dialog';
import {SettingsDialogComponent} from './settingsDialog/settingsDialogComponent';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent {

  userAccount: UserAccount = new UserAccount(Number(''), ' ', ' ', new Date(), ' ');

  constructor(private homeService: HomeService, private matDialog: MatDialog) {
    this.fetchUserData();
  }

  private fetchUserData(): void {
    this.homeService.getAccountDetails().subscribe(
      response => {
        this.userAccount = response;
      },
      error => alert('err'));
  }


  openDialog(): void {
    const dialog = this.matDialog.open(SettingsDialogComponent, {
      data: this.userAccount,
    });
    dialog.afterClosed().subscribe((userAccountValues) => {
      if (!userAccountValues) {
        return;
      }
      this.updateUserAccount(userAccountValues);
    });
  }

  private updateUserAccount(userAccountValues: UserAccount): void {
    this.homeService.updateUserDetails(userAccountValues).subscribe(
      response => {
        this.userAccount = response;
      },
      error => alert('Error updating user details')
    );
  }
}
