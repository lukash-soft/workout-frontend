import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {Event} from '../model/Event';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  private EVENT_URL = `${environment.baseUrl}/event`;

  constructor(private http: HttpClient) {
  }

  public getEventsForUser(): Observable<Event[]> {
    return this.http.get<Event[]>(`${this.EVENT_URL}`);
  }

  public addEvent(createdEvent: Event): Observable<Event> {
    return this.http.post<Event>(this.EVENT_URL, createdEvent);
  }

  public deleteEvent(eventId: number): Observable<void> {
    const deleteUrl = `${this.EVENT_URL}/${eventId}`;
    return this.http.delete<void>(deleteUrl);
  }

  public updateEvent(eventToUpdate: Event): Observable<Event> {
    return this.http.put<Event>(this.EVENT_URL, eventToUpdate);
  }

}
