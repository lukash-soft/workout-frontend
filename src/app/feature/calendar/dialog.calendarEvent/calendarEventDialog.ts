import {Component, Inject, KeyValueDiffers} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Event} from '../model/Event';
import {EventFormWrapper} from './event-form-wrapper';

@Component({
  selector: 'app-calendar-event-dialog',
  templateUrl: './calendarEventDialog.html',
})
export class CalendarEventDialogComponent {
  formCalendar: FormGroup;
  startDateFormControl = new FormControl('', Validators.required);
  endDateFormControl = new FormControl('', Validators.required);

  constructor(@Inject(MAT_DIALOG_DATA) public eventModel: Event,
              private dialogRef: MatDialogRef<CalendarEventDialogComponent>) {
    if (eventModel == null) {
      this.prepareEmptyForm();
    } else {
      this.fillFormWithExistingEvent(eventModel);
    }
  }

  private fillFormWithExistingEvent(eventModel: Event): void {
    this.startDateFormControl.setValue(eventModel.start);
    this.endDateFormControl.setValue(eventModel.end);

    this.formCalendar = new FormGroup({
      description: new FormControl(eventModel.description, Validators.required),
      title: new FormControl(eventModel.title, Validators.required),
      start: this.startDateFormControl,
      end: this.endDateFormControl
    });
  }

  private prepareEmptyForm(): void {
    this.formCalendar = new FormGroup({
      description: new FormControl('', Validators.required),
      title: new FormControl('', Validators.required),
      start: this.startDateFormControl,
      end: this.endDateFormControl,
    });
  }

  completeForm(): void {
    const eventId = this.eventModel !== null ? this.eventModel.id : null;

    const eventToSave = Event.EventForm(
      // @ts-ignore
      eventId,
      this.formCalendar.controls.description.value,
      this.formCalendar.controls.title.value,
      this.formCalendar.controls.start.value,
      this.formCalendar.controls.end.value,
    );
    const formResult = EventFormWrapper.eventToSave(eventToSave);

    this.dialogRef.close(formResult);
  }

  onDelete(): void {
    const formToDelete = EventFormWrapper.eventToDelete(this.eventModel);
    this.dialogRef.close(formToDelete);
  }
}
