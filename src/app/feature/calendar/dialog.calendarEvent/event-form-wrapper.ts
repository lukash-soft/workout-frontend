import {Event} from '../model/Event';

export class EventFormWrapper {

  readonly event: Event;
  readonly isForDeletion: boolean;
  readonly isForEdit: boolean;

  private constructor(event: Event, isForDeletion: boolean, isForEdit: boolean) {
    this.event = event;
    this.isForDeletion = isForDeletion;
    this.isForEdit = isForEdit;
  }

  static eventToDelete(event: Event): EventFormWrapper {
    return new EventFormWrapper(event, true, false);
  }

  static eventToSave(event: Event): EventFormWrapper {
    const isForEdit = event.id !== null ? true : false;

    return new EventFormWrapper(event, false, isForEdit);
  }
}
