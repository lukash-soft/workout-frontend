import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';

import {CalendarEvent, CalendarView} from 'angular-calendar';
import {CalendarService} from './service/calendar.service';
import {MatDialog} from '@angular/material/dialog';
import {CalendarEventDialogComponent} from './dialog.calendarEvent/calendarEventDialog';
import {Event} from './model/Event';
import {readSpanComment} from '@angular/compiler-cli/src/ngtsc/typecheck/src/comments';
import {EventFormWrapper} from './dialog.calendarEvent/event-form-wrapper';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  }
};

@Component({
  selector: 'app-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})

export class CalendarComponent implements OnInit {
  view: CalendarView = CalendarView.Week;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  refresh: Subject<any> = new Subject();
  activeDayIsOpen = true;

  events: CalendarEvent[] = [];

  constructor(private calendarService: CalendarService, private dialog: MatDialog) {
  }

  setView(view: CalendarView): void {
    this.view = view;
  }

  closeOpenMonthViewDay(): void {
    this.activeDayIsOpen = false;
  }

  ngOnInit(): void {
    this.calendarService.getEventsForUser().subscribe(
      events => {
        this.events = events;
        this.refreshEvents();
      },
      error => console.error(error));
  }

  private refreshEvents(): void {
    this.reformatDates();
    this.refresh.next();
  }

  openAddEventDialog(): void {
    const dialogRef = this.dialog.open(CalendarEventDialogComponent);
    dialogRef.afterClosed().subscribe(
      (eventToAdd: EventFormWrapper) => {
        if (!eventToAdd) {
          return;
        }
        this.createEvent(eventToAdd.event);
      }
    );
  }

  viewEventDetails({event}: { event: CalendarEvent }): void {
    const dialogRef = this.dialog.open(CalendarEventDialogComponent, {
      data: event
    });
    dialogRef.afterClosed().subscribe(
      (formResult: EventFormWrapper) => {
        if (!formResult) {
          return;
        }

        if (formResult.isForDeletion) {
          this.deleteEvent(formResult);
        } else if (formResult.isForEdit) {
          this.updateEvent(formResult);
        }
      }
    );
  }

  private deleteEvent(formResult: EventFormWrapper): void {
    this.calendarService.deleteEvent(formResult.event.id).subscribe(
      response => {
        for (let i = 0; i < this.events.length; i++) {
          if (this.events[i].id === formResult.event.id) {
            this.events.splice(i, 1);
            this.refreshEvents();
            alert('Successful deletion');
            break;
          }
        }
      }, error => alert('Error deleting')
    );
  }

  private updateEvent(formResult: EventFormWrapper): void {
    this.calendarService.updateEvent(formResult.event).subscribe(
      response => {
        for (let i = 0; i < this.events.length; i++) {
          if (this.events[i].id === formResult.event.id) {
            this.events.splice(i, 1, response);
            this.refreshEvents();
            break;
          }
        }
      },
      error => alert('Error updating events')
    );
  }

  private createEvent(eventToCreate: Event): void {
    this.calendarService.addEvent(eventToCreate).subscribe(
      response => {
        this.events.push(response);
      },
      error => alert('Something is wrong')
    );
  }

  private reformatDates(): void {
    this.events.forEach(e => {
      e.start = new Date(e.start);
      e.end ? e.end = new Date(e.end) : e.end = undefined;
    });
  }
}
