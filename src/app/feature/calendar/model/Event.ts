import {CalendarEvent} from 'angular-calendar';
import {UserAccountDto} from './UserAccountDto';

export class Event implements CalendarEvent {

  id: number;
  description: string;
  title: string;
  start: Date;
  end: Date;
  createdBy: UserAccountDto;
  participants: UserAccountDto[];

  constructor() {
  }

  static EventForm(id: number, description: string, title: string, start: Date, end: Date): Event {
    const eventForm = new Event();

    eventForm.id = id;
    eventForm.description = description;
    eventForm.title = title;
    eventForm.start = start;
    eventForm.end = end;

    return eventForm;
  }

}
