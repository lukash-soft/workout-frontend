import {UserAccount} from '../../home/model/user-account';

export class Group {

  id: number;
  groupName: string;
  groupType: string;
  admin: UserAccount;
  members: UserAccount[];


  constructor() {
  }

  static GroupForm(id: number, groupName: string, groupType: string): Group {
    const groupForm = new Group();

    groupForm.id = id;
    groupForm.groupName = groupName;
    groupForm.groupType = groupType;

    return groupForm;
  }

  static emptyGroup(): Group {
    return new Group();
  }

}
