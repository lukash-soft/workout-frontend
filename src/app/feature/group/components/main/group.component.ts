import {Component} from '@angular/core';
import {Group} from '../../model/group';
import {GroupService} from '../../service/group.Service';
import {MatDialog} from '@angular/material/dialog';
import {AddGroupDialogComponent} from '../../dialog/addGroupDialog/addGroupDialog';
import {UpdateGroupDialogComponent} from '../../dialog/updateGroupDialog/updateGroupDialog';
import {UserAccount} from '../../../home/model/user-account';
import {AddMemberDialogComponent} from '../../dialog/addMemberDialog/addMemberDialog';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent {

  groups: Group[] = [];
  isDetailedView = false;
  groupChosen: Group = Group.emptyGroup();
  members: UserAccount[] = [];

  constructor(private groupService: GroupService, private matDialog: MatDialog) {
    this.getAllGroupsForUser();
  }

  getAllGroupsForUser(): void {
    this.groupService.getAllGroupsForLoggedUser().subscribe(
      response => {
        this.groups = response;
      },
      error => alert('Error getting groups')
    );
  }

  openAddGroupDialog(): void {
    const dialogRef = this.matDialog.open(AddGroupDialogComponent);
    dialogRef.afterClosed().subscribe(
      (groupToAdd) => {
        if (!groupToAdd) {
          return;
        }
        this.addGroup(groupToAdd);
      }
    );
  }

  selectGroup(chosenGroup: Group): void {
    this.groupChosen = chosenGroup;
    this.isDetailedView = true;
  }

  clearGroupChoice(): void {
    this.isDetailedView = false;
    this.groupChosen = Group.emptyGroup();

  }

  private addGroup(groupToAdd: Group): void {
    this.groupService.addGroup(groupToAdd).subscribe(
      response => {
        this.groups.push(response);
      },
      error => alert('Error adding group')
    );
  }

  openMemberDialog(groupId: number): void {
    const dialogRef = this.matDialog.open(AddMemberDialogComponent);
    dialogRef.afterClosed().subscribe(
      (memberToAdd) =>
        this.addMember(memberToAdd.id, groupId)
    );
  }

  private addMember(memberId: number, groupId: number): void {
    this.groupService.addMember(memberId, groupId).subscribe(
      response => {
        this.members.push(response);
      },
      error => alert('Error while adding friend')
    );
  }

  removeGroup(groupToRemoveId: number): void {
    this.groupService.removeGroup(groupToRemoveId).subscribe(
      response => {
        for (let i = 0; i < this.groups.length; i++) {
          if (this.groups[i].id === groupToRemoveId) {
            this.groups.splice(i, 1);
            break;
          }
        }
      }
    );
  }

  openUpdateGroupDialog(groupToUpdate: Group): void {
    const dialogRef = this.matDialog.open(UpdateGroupDialogComponent, {
      data: groupToUpdate
    });
    dialogRef.afterClosed().subscribe(
      (editedGroup) => {
        if (!editedGroup) {
          return;
        }
        this.groupService.updateGroup(editedGroup).subscribe(
          response => {
            for (let i = 0; i < this.groups.length; i++) {
              if (this.groups[i].id === editedGroup.id) {
                this.groups.splice(i, 1, response);
                break;
              }
            }
          },
          error => alert('Error updating group')
        );
      });
  }
}
