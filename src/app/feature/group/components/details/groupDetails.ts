import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {Group} from '../../model/group';
import {GroupService} from '../../service/group.Service';
import {UserAccount} from '../../../home/model/user-account';
import {readSpanComment} from '@angular/compiler-cli/src/ngtsc/typecheck/src/comments';

@Component({
  selector: `app-group-details`,
  templateUrl: `./groupDetails.html`
})
export class GroupDetailsComponent implements OnChanges {
  @Input()
  group!: Group;
  members: UserAccount[] = [];
  @Output()
  clearEmitter: EventEmitter<void> = new EventEmitter<void>();

  constructor(private groupService: GroupService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getMembersForAGroup(this.group.id);
  }

  clearChoice(): void {
    this.clearEmitter.emit();
  }

  getMembersForAGroup(chosenGroupId: number): void {
    this.groupService.getMembersForAGroup(chosenGroupId).subscribe(
      response => {
        this.members = response;
      },
      error => alert('Error getting members')
    );
  }

  removeMember(memberId: number, groupId: number): void {
    this.groupService.removeMember(memberId, groupId).subscribe(
      response => {
        for (let i = 0; i < this.members.length; i++) {
          if (this.members[i].id === memberId) {
            this.members.splice(i, 1);
            break;
          }
        }
      }
    );
  }
}
