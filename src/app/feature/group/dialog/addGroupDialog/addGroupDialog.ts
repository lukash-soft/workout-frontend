import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {Group} from '../../model/group';

@Component({
    selector: 'app-add-group-dialog',
    templateUrl: './addGroupDialog.html',
  }
)

export class AddGroupDialogComponent {

  formAddGroup: FormGroup;


  constructor(private matDialog: MatDialogRef<AddGroupDialogComponent>) {
    this.formAddGroup = new FormGroup({
      id: new FormControl('', Validators.required),
      groupName: new FormControl('', Validators.required),
      groupType: new FormControl('', Validators.required),
      }
    );
  }

  addGroup(): void {
    const groupToAdd = Group.GroupForm(
      this.formAddGroup.controls.id.value,
      this.formAddGroup.controls.groupName.value,
      this.formAddGroup.controls.groupType.value,
    );
    this.matDialog.close(groupToAdd);
  }
}
