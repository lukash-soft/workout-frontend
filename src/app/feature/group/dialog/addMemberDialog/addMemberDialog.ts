import {Component} from '@angular/core';
import {GroupService} from '../../service/group.Service';
import {MatDialogRef} from '@angular/material/dialog';
import {UserAccount} from '../../../home/model/user-account';
import {Group} from '../../model/group';


@Component({
    selector: 'app-add-member-dialog',
    templateUrl: './addMemberDialog.html',
  }
)

export class AddMemberDialogComponent {

  clients: UserAccount[] = [];
  group: Group;

  constructor(private groupService: GroupService, private matDialogRef: MatDialogRef<AddMemberDialogComponent>) {
    this.getClients();
  }

  getClients(): void {
    this.groupService.getClients().subscribe(
      response =>
        this.clients = response
    );
  }

  addMember(memberId: number): void {
    const memberToAdd = this.clients.find(client => client.id === memberId);
    this.matDialogRef.close(memberToAdd);
  }


}
