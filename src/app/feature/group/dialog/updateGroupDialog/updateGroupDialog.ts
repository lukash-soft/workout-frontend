import {Component, Inject} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Group} from '../../model/group';

@Component({
    selector: 'app-update-group-dialog',
    templateUrl: './updateGroupDialog.html',
  }
)
export class UpdateGroupDialogComponent {

  formUpdateGroup: FormGroup;


  constructor(@Inject(MAT_DIALOG_DATA) private data: Group,
              private matDialogRef: MatDialogRef<UpdateGroupDialogComponent>) {
    this.formUpdateGroup = new FormGroup({
      id: new FormControl(data.id),
      groupName: new FormControl(data.groupName),
      groupType: new FormControl(data.groupType)
    });
  }

  onUpdateAndSave(): void {
    const groupToUpdate = Group.GroupForm(
      this.formUpdateGroup.controls.id.value,
      this.formUpdateGroup.controls.groupName.value,
      this.formUpdateGroup.controls.groupType.value
    );
    this.matDialogRef.close(groupToUpdate);
  }
}
