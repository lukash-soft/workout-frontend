import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Group} from '../model/group';
import {UserAccount} from '../../home/model/user-account';

@Injectable({
  providedIn: 'root'
})

export class GroupService {
  private GROUP_URL = `${environment.baseUrl}/group/user`;
  private ALL_GROUPS_URL = `${environment.baseUrl}/group`;
  private USER_URL = `${environment.baseUrl}/accounts/clients`;

  constructor(private http: HttpClient) {
  }

  public getAllGroupsForLoggedUser(): Observable<Group[]> {
    return this.http.get<Group[]>(this.GROUP_URL);
  }

  public getAllGroups(): Observable<Group[]> {
    return this.http.get<Group[]>(this.ALL_GROUPS_URL);
  }

  public addGroup(groupToAdd: Group): Observable<Group> {
    return this.http.post<Group>(this.ALL_GROUPS_URL, groupToAdd);
  }

  public removeGroup(groupId: number): Observable<void> {
    const removeUrl = `${this.ALL_GROUPS_URL}/${groupId}`;
    return this.http.delete<void>(removeUrl);
  }

  public updateGroup(groupToUpdate: Group): Observable<Group> {
    return this.http.put<Group>(this.ALL_GROUPS_URL, groupToUpdate);
  }

  public getMembersForAGroup(groupId: number): Observable<UserAccount[]> {
    const getMembersUrl = `${this.ALL_GROUPS_URL}/${groupId}`;
    return this.http.get<UserAccount[]>(getMembersUrl);
  }

  public getClients(): Observable<UserAccount[]> {
    return this.http.get<UserAccount[]>(this.USER_URL);
  }

  public addMember(memberId: number, groupId: number): Observable<UserAccount> {
    const addMemberUrl = `${this.ALL_GROUPS_URL}/add/member/${memberId}/group/${groupId}`;
    return this.http.put<UserAccount>(addMemberUrl, null);
  }

  public removeMember(memberId: number, groupId: number): Observable<void> {
    const removeMemberUrl = `${this.ALL_GROUPS_URL}/remove/member/${memberId}/group/${groupId}`;
    return this.http.put<void>(removeMemberUrl, null);
  }

}
