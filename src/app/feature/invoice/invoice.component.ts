import {Component, OnInit, ViewChild} from '@angular/core';
import {InvoiceService} from './service/invoice.service';
import {Invoice} from './model/invoice';
import {MatTable} from '@angular/material/table';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent {

  columnsToDisplay = ['firstName', 'lastName', 'toPay', 'created', 'isPaid', 'payInvoice'];
  invoices: Invoice[] = [];

  @ViewChild('invoiceTable') invoicesTable: MatTable<any>;

  constructor(private invoiceService: InvoiceService) {
    this.getAllInvoices();
  }

  printPaymentStatus(invoice: Invoice): string {
    if (invoice.isPaid) {
      return 'Yes';
    } else {
      return 'No';
    }
  }

  getAllInvoices(): void {
    this.invoiceService.getInvoices().subscribe(
      response => {
        this.invoices = response;
      },
      error => alert('Error finding invoices.')
    );
  }
}
