import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Invoice} from '../model/invoice';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  private INVOICE_URL = `${environment.baseUrl}/invoice`;

  constructor(private http: HttpClient) {
  }

  public getInvoices(): Observable<Invoice[]> {
    return this.http.get<Invoice[]>(this.INVOICE_URL);
  }
}
