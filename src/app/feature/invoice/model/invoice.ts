import {UserAccountDto} from '../../calendar/model/UserAccountDto';

export class Invoice {

  id: number;
  createdDay: Date;
  user: UserAccountDto;
  totalAmount: number;
  isPaid: boolean;


  constructor(id: number, createdDay: Date, user: UserAccountDto, totalAmount: number, isPaid: boolean) {
    this.id = id;
    this.createdDay = createdDay;
    this.user = user;
    this.totalAmount = totalAmount;
    this.isPaid = isPaid;
  }
}
