import {Component} from '@angular/core';
import {SubscriptionService} from './service/subscription.service';
import {Subscription} from './model/subscription';
import {MatDialog} from '@angular/material/dialog';
import {SubscriptionUpdateDialogComponent} from './updateSubscriptionDialog/subscriptionUpdateDialog';
import {UserAccount} from '../home/model/user-account';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent {

  subscriptions: Subscription[] = [];
  friends: UserAccount[] = [];

  constructor(private subscriptionService: SubscriptionService, private matDialog: MatDialog) {
    this.getAllSubs();
  }

  getAllSubs(): void {
    this.subscriptionService.getAllSubscription().subscribe(
      response => {
        this.subscriptions = response;
      },
      error => alert('Error finding subs.')
    );
  }

  cancelSubscription(subId: number): void {
    this.subscriptionService.cancelSubscription(subId).subscribe(
      response => {
        for (let i = 0; i < this.subscriptions.length; i++) {
          if (this.subscriptions[i].id === subId) {
            this.subscriptions.splice(i, 1, response);
          }
        }
      },
      error => alert('Error canceling subscription')
    );
  }

  openUpdateSubDialog(subToUpdate: Subscription): void {
    const dialogRef = this.matDialog.open(SubscriptionUpdateDialogComponent, {
        data: subToUpdate
      }
    );
    dialogRef.afterClosed().subscribe(
      (editedSub) => {
        this.subscriptionService.updateSubscriptionFee(editedSub).subscribe(
          response => {
            for (let i = 0; i < this.subscriptions.length; i++) {
              if (this.subscriptions[i].id === editedSub.id) {
                this.subscriptions.splice(i, 1, response);
                break;
              }
            }
          },
          error => alert('Error updating fee.')
        );
      }
    );
  }


  removeSubscription(subId: number): void {
    this.subscriptionService.removeSubscription(subId).subscribe(
      response => {
        for (let i = 0; i < this.subscriptions.length; i++) {
          if (this.subscriptions[i].id === subId) {
            this.subscriptions.splice(i, 1);
            break;
          }
        }
      },
      error => alert('Error removing sub.')
    );
  }


}
