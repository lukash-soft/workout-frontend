import {Component, Inject} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Subscription} from '../model/subscription';
import {UserAccountDto} from '../../calendar/model/UserAccountDto';
import {SubscriptionService} from '../service/subscription.service';
import {FriendsService} from '../../friends/service/friends.service';
import {UserAccount} from '../../home/model/user-account';

@Component({
  selector: 'app-sub-dialog',
  templateUrl: './subscriptionUpdateDialog.html',
})
export class SubscriptionUpdateDialogComponent {

  formSubGroup: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public subscriptionModel: Subscription,
              private matDialogRef: MatDialogRef<SubscriptionUpdateDialogComponent>) {
      this.fillWithExistingSubscription(subscriptionModel);
  }

  private fillWithExistingSubscription(subscriptionModel: Subscription): void {
    this.formSubGroup = new FormGroup({
      id: new FormControl(subscriptionModel.id),
      coach: new FormControl(subscriptionModel.coach),
      client: new FormControl(subscriptionModel.client),
      created: new FormControl(subscriptionModel.created),
      ended: new FormControl(subscriptionModel.ended),
      fee: new FormControl(subscriptionModel.fee),
    });
  }

  onSave(): void {
    const subToUpdate = new Subscription(
      this.formSubGroup.controls.id.value,
      this.formSubGroup.controls.coach.value,
      this.formSubGroup.controls.client.value,
      this.formSubGroup.controls.created.value,
      this.formSubGroup.controls.ended.value,
      this.formSubGroup.controls.fee.value,
    );
    this.matDialogRef.close(subToUpdate);
  }

}
