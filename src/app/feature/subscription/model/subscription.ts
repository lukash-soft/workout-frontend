import {UserAccountDto} from '../../calendar/model/UserAccountDto';

export class Subscription {

  id: number;
  coach: UserAccountDto;
  client: UserAccountDto;
  created: Date;
  ended: Date;
  fee: number;

  constructor(id: number, coach: UserAccountDto, client: UserAccountDto, created: Date, ended: Date, fee: number) {
    this.id = id;
    this.coach = coach;
    this.client = client;
    this.created = created;
    this.ended = ended;
    this.fee = fee;
  }
}
