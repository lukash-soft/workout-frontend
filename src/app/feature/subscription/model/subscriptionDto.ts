export class SubscriptionDto {

  clientId: number;
  fee: number;

  constructor(clientId: number, fee: number) {
    this.clientId = clientId;
    this.fee = fee;
  }
}
