import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {Subscription} from '../model/subscription';
import {OfferDto} from '../../offer/model/offerDto';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  private BASE_URL = `${environment.baseUrl}/subscription`;

  constructor(private http: HttpClient) {
  }

  public getAllSubscription(): Observable<Subscription[]> {
    return this.http.get<Subscription[]>(this.BASE_URL);
  }

  public createSubscription(offerDto: OfferDto): Observable<Subscription> {
    return this.http.post<Subscription>(this.BASE_URL, offerDto);
  }

  public updateSubscriptionFee(subscription: Subscription): Observable<Subscription> {
    return this.http.put<Subscription>(`${this.BASE_URL}/fee`, subscription);
  }

  public cancelSubscription(subscriptionId: number): Observable<Subscription> {
    return this.http.put<Subscription>(`${this.BASE_URL}/cancel/${subscriptionId}`, null);
  }

  public removeSubscription(subId: number): Observable<void> {
    return this.http.delete<void>(`${this.BASE_URL}/${subId}`);
  }

}
