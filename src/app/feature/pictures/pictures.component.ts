import {Component, OnInit} from '@angular/core';
import {HomeService} from '../home/service/home.service';
import {PictureService} from './service/picture.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-pictures',
  templateUrl: './pictures.component.html',
  styleUrls: ['./pictures.component.css']
})
export class PicturesComponent implements OnInit {

  pictures: any[] = [];

  constructor(private pictureService: PictureService, private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.loadPictures();
  }

  loadPictures(): void {
    this.pictureService.getPicturesIds().subscribe(
      pictureIds => {
        pictureIds.forEach(id => this.getPicById(id));
      },
      error => {
        console.log('Pictures not found');
      }
    );
  }

  private getPicById(id: number): void {
    this.pictureService.getPictureById(id).subscribe(
      response => {
        if (response.size > 0) {
          const imageSanitized = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(response));
          this.pictures.push(imageSanitized);
        }
      },
      err => {
        console.error(err);
      }
    );
  }
}
