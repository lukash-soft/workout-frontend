import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PictureService {
  private readonly PICTURE_URL = `${environment.baseUrl}/pictures`;

  constructor(private  http: HttpClient) {
  }

  public getPictureById(id: number): Observable<Blob> {
    return this.http.get(`${this.PICTURE_URL}/${id}`, {responseType: 'blob'});
  }

  public getPicturesIds(): Observable<number[]> {
    return this.http.get<number[]>(this.PICTURE_URL);
  }
}
