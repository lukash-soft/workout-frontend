import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../../configuration/security/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  private url = environment.baseUrl;
  form: FormGroup;

  constructor(private http: HttpClient, private snackBar: MatSnackBar, private authService: AuthenticationService, private router: Router) {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    });
  }

  onRegister(): void {
    this.router.navigate(['/register']);
  }

  onAuthorize(): void {
    const email = this.form.controls.email.value;
    const password = this.form.controls.password.value;
    this.fetchToken(email, password).subscribe(
      (response: HttpResponse<any>) => this.processSuccessfulAttempt(response),
      error => this.processFailedAttempt(error)
    );
  }

  private processFailedAttempt(error: any): void {
    const message = error.status === 403 ? 'Wrong credentials' : 'Server down';

    this.showErrorMessage(message);
  }

  private showErrorMessage(message: string): void {
    this.snackBar.open(`Could not authorise: ${message}`, '', {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: ['red-snackbar']
    });
  }

  private fetchToken(email: string, password: string): Observable<HttpResponse<any>> {
    const credentials = {email, password};
    const contentHeader = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post<HttpResponse<any>>(`${this.url}/login`, credentials, {
      headers: contentHeader,
      observe: 'response'
    });
  }

  private processSuccessfulAttempt(response: HttpResponse<any>): void {
    const authStatus = this.authService.authenticate(response);
    if (!authStatus.authenticated) {
      this.showErrorMessage(authStatus.reason);
    }
  }
}
