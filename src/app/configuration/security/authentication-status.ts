export class AuthenticationStatus {
  authenticated: boolean;
  reason: string;


  constructor(authenticated: boolean, reason: string) {
    this.authenticated = authenticated;
    this.reason = reason;
  }

  public static failed(reason: string): AuthenticationStatus {
    return new AuthenticationStatus(false, reason);
  }

  public static ok(): AuthenticationStatus {
    return new AuthenticationStatus(true, 'OK');
  }
}
