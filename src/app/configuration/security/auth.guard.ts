import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {TokenStorage} from './token.storage';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private token: TokenStorage, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.token.getToken()) {
      return true;
    }
    // not logged in so redirect to login page
    this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
    return false;
  }
}
