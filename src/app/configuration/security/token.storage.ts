import {Injectable} from '@angular/core';
import {Token} from './token';
import {BehaviorSubject} from 'rxjs';

const ROLE = 'role';
const TOKEN_KEY = 'authorization';

@Injectable()
export class TokenStorage {

  userRole$: BehaviorSubject<string> = new BehaviorSubject<string>(this.getRole());
  isLoggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(!!this.getRole());

  constructor() {
  }

  signOut(): void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.removeItem(ROLE);
    window.sessionStorage.clear();

    this.isLoggedIn$.next(false);
    this.userRole$.next('');
  }

  public getRole(): string {
    return sessionStorage.getItem(ROLE) as string;
  }

  public saveToken(token: Token): void {
    window.sessionStorage.setItem(ROLE, token.role);
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, JSON.stringify(token));

    this.isLoggedIn$.next(true);
    this.userRole$.next(token.role);
  }

  public getToken(): Token {
    return JSON.parse(sessionStorage.getItem(TOKEN_KEY) as string);
  }

}
