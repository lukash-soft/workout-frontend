import {HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationStatus} from './authentication-status';
import {Token} from './token';
import {TokenStorage} from './token.storage';
import jwtDecode from 'jwt-decode';

@Injectable()
export class AuthenticationService {

  constructor(private router: Router, private tokenStorage: TokenStorage) {
  }

  authenticate(response: HttpResponse<any>): AuthenticationStatus {
    const jwt = response.headers.get('authorization');

    if (!jwt) {
      this.router.navigate(['']);
      return AuthenticationStatus.failed('Token missing');
    }
    const decoded = jwtDecode<any>(jwt);
    const token: Token = new Token(decoded.sub, jwt, decoded.roles);

    this.tokenStorage.saveToken(token);
    this.router.navigate(['/home']);
    return AuthenticationStatus.ok();
  }

  signout(): void {
    this.tokenStorage.signOut();
    this.router.navigate(['/']);
  }
}
