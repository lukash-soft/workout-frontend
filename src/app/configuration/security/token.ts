export class Token {
  name: string;
  jwt: string;
  role: string;

  constructor(name: string, jwt: string, role: string) {
    this.name = name;
    this.jwt = jwt;
    this.role = role;
  }
}
