import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './feature/home/home.component';
import {AuthGuard} from './configuration/security/auth.guard';
import {LoginComponent} from './feature/login/login.component';
import {RegisterComponent} from './feature/register/register.component';
import {ProfileComponent} from './feature/profile/profile.component';
import {FriendsComponent} from './feature/friends/friends.component';
import {InvitationComponent} from './feature/invitation/invitation.component';
import {GroupComponent} from './feature/group/components/main/group.component';
import {GroupDetailsComponent} from './feature/group/components/details/groupDetails';
import {PicturesComponent} from './feature/pictures/pictures.component';
import {CalendarComponent} from './feature/calendar/calendar.component';
import {SettingsComponent} from './feature/settings/settings.component';
import {InvoiceComponent} from './feature/invoice/invoice.component';
import {SubscriptionComponent} from './feature/subscription/subscription.component';
import {OfferComponent} from './feature/offer/coach/offer.component';
import {ClientOfferComponent} from './feature/offer/client/client-offer.component';
import {MockPaymentPageComponent} from './feature/mock-payment-page/mock-payment-page.component';

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  { path: 'register', component: RegisterComponent},
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'friends', component: FriendsComponent, canActivate: [AuthGuard]},
  {path: 'invitations', component: InvitationComponent, canActivate: [AuthGuard]},
  {path: 'pictures', component: PicturesComponent, canActivate: [AuthGuard]},
  {path: 'groups', component: GroupComponent, canActivate: [AuthGuard]},
  {path: 'groupsDetails', component: GroupDetailsComponent, canActivate: [AuthGuard]},
  {path: 'calendar', component: CalendarComponent, canActivate: [AuthGuard]},
  {path: 'settings', component: SettingsComponent, canActivate: [AuthGuard]},
  {path: 'invoice', component: InvoiceComponent, canActivate: [AuthGuard]},
  {path: 'subscription', component: SubscriptionComponent, canActivate: [AuthGuard]},
  {path: 'offer', component: OfferComponent, canActivate: [AuthGuard]},
  {path: 'client', component: ClientOfferComponent, canActivate: [AuthGuard]},
  {path: 'mockPaymentPage', component: MockPaymentPageComponent, canActivate: [AuthGuard]},
  { path: '**', component: HomeComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
