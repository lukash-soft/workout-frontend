import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './feature/login/login.component';
import {HomeComponent} from './feature/home/home.component';
import {RegisterComponent} from './feature/register/register.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {AuthGuard} from './configuration/security/auth.guard';
import {TokenStorage} from './configuration/security/token.storage';
import {AuthenticationService} from './configuration/security/authentication.service';
import {Interceptor} from './configuration/security/inteceptor';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule, MatOptionModule} from '@angular/material/core';
import {NavBarComponent} from './feature/navigationBar/nav-bar.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {ProfileComponent} from './feature/profile/profile.component';
import {AthleteDetailsDialogUpdateComponent} from './feature/profile/dialog.athleteDetails/updateDetailsDialog/athleteDetailsDialogUpdate';
import {MatSelectModule} from '@angular/material/select';
import {MAT_DIALOG_DATA, MatDialogModule} from '@angular/material/dialog';
import {FriendsComponent} from './feature/friends/friends.component';
import {MatTableModule} from '@angular/material/table';
import {InvitationComponent} from './feature/invitation/invitation.component';
import {MatCardModule} from '@angular/material/card';
import {CreatePostDialogComponent} from './feature/home/dialog.models/dialog.createPost/createPostDialog';
import {UpdatePostDialogComponent} from './feature/home/dialog.models/dialog.updatePost/updatePostDialog';
import {FlexLayoutModule} from '@angular/flex-layout';
import {GroupComponent} from './feature/group/components/main/group.component';
import {AddGroupDialogComponent} from './feature/group/dialog/addGroupDialog/addGroupDialog';
import {UpdateGroupDialogComponent} from './feature/group/dialog/updateGroupDialog/updateGroupDialog';
import {GroupDetailsComponent} from './feature/group/components/details/groupDetails';
import {DateFormatPostPipe} from './feature/home/dateFormatPost.pipe';
import {FileUploadModule} from 'ng2-file-upload';
import {PicturesComponent} from './feature/pictures/pictures.component';
import {CalendarComponent} from './feature/calendar/calendar.component';
import {CommonModule} from '@angular/common';
import {FlatpickrModule} from 'angularx-flatpickr';
import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import {CalendarEventDialogComponent} from './feature/calendar/dialog.calendarEvent/calendarEventDialog';
import {NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule} from '@angular-material-components/datetime-picker';
import {SettingsComponent} from './feature/settings/settings.component';
import {SettingsDialogComponent} from './feature/settings/settingsDialog/settingsDialogComponent';
import { InvoiceComponent } from './feature/invoice/invoice.component';
import { SubscriptionComponent } from './feature/subscription/subscription.component';
import {SubscriptionUpdateDialogComponent} from './feature/subscription/updateSubscriptionDialog/subscriptionUpdateDialog';
import { OfferComponent } from './feature/offer/coach/offer.component';
import {OfferDialogComponent} from './feature/offer/coach/dialog/offerDialogComponent';
import { ClientOfferComponent } from './feature/offer/client/client-offer.component';
import { MockPaymentPageComponent } from './feature/mock-payment-page/mock-payment-page.component';
import {AddMemberDialogComponent} from './feature/group/dialog/addMemberDialog/addMemberDialog';
import {AddFriendDialogComponent} from './feature/invitation/addFriendDialog/addFriendDailog';
import {AthleteDetailsDialogAddComponent} from './feature/profile/dialog.athleteDetails/addAthleteDetails/athlete-details-dialog-add.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    NavBarComponent,
    ProfileComponent,
    AthleteDetailsDialogUpdateComponent,
    FriendsComponent,
    InvitationComponent,
    CreatePostDialogComponent,
    UpdatePostDialogComponent,
    GroupComponent,
    AddGroupDialogComponent,
    UpdateGroupDialogComponent,
    GroupDetailsComponent,
    DateFormatPostPipe,
    PicturesComponent,
    CalendarComponent,
    CalendarEventDialogComponent,
    SettingsComponent,
    SettingsDialogComponent,
    InvoiceComponent,
    SubscriptionComponent,
    SubscriptionUpdateDialogComponent,
    OfferComponent,
    OfferDialogComponent,
    ClientOfferComponent,
    MockPaymentPageComponent,
    AddMemberDialogComponent,
    AddFriendDialogComponent,
    AthleteDetailsDialogAddComponent
  ],
  imports: [
    BrowserAnimationsModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatToolbarModule,
    MatMenuModule,
    MatOptionModule,
    MatSelectModule,
    MatDialogModule,
    MatTableModule,
    MatCardModule,
    FlexLayoutModule,
    FileUploadModule,
    CommonModule,
    FormsModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
  ],
  entryComponents: [
    AthleteDetailsDialogUpdateComponent,
  ],
  providers: [
    {provide: MAT_DIALOG_DATA, useValue: {hasBackdrop: false}},
    AuthGuard,
    TokenStorage,
    AuthenticationService,
    LoginComponent,
    {provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true},
    {provide: MAT_DATE_FORMATS, useValue: {}},
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: 'YYYY-MM-DD',
        },
        display: {
          dateInput: 'YYYY-MM-DD',
          monthYearLabel: 'YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'YYYY',
        },
      },
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
